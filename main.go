package main

import (
	"os"
	"fmt"
	"bufio"
	"regexp"
	"strings"
)

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func setTocTitle(text string) string {
	var link = "**[" + strings.Trim(text, "# ") + "]"
	var markup = "(#" + strings.Replace(strings.ToLower(strings.Trim(text, "# ")), " ", "-", -1) + ")**\n"

	return "\n" + link + markup + "\n"
}

func setTocEntry(level int, text string) string {
	lvl := make(map[int]string)		
	
	lvl[3] = "* "
	lvl[4] = "  * "
	lvl[5] = "    * "
	lvl[6] = "      * "

	var link = lvl[level] + "[" + text + "]"
	var markup = "(#" + strings.Replace(strings.ToLower(text), " ", "-", -1) + ")\n"

	return link + markup
}

func main() {

	var md_file = os.Args[1]
	md, err := os.Open(md_file)
	check(err)

	fmt.Println("## Table of Contents")

	r, _ := regexp.Compile("^##*")

	scanner := bufio.NewScanner(md)
	for scanner.Scan() {
		if strings.HasPrefix(scanner.Text(), "# ") {
			fmt.Println(setTocTitle(strings.Trim(scanner.Text(), "# ")))
		} else if r.MatchString(scanner.Text()) {
			fmt.Println(setTocEntry(len(strings.Fields(scanner.Text())[0]), strings.Trim(scanner.Text(), strings.Fields(scanner.Text())[0] + " ")))
		}
	}
}