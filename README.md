# md_toc_generator

Simple Markdown Table Of Content Generator

## Table of Contents

* [Summary](#summary)
* [Usage](#usage)

## Summary

Table of Contents generator for Markdown files

## Usage

```bash
./md_toc_generator README.md
```

## Result example

![](https://gitlab.com/julienlevasseur/md_toc_generator/raw/master/demo.png)